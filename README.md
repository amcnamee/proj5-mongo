# Project 5: Brevet time calculator with Ajax and MongoDB

Simple list of controle times from project 4 stored in MongoDB database.

# Project 5: Brevet time calculator with Ajax stored in MongoDB database

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

## ACP controle times

Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.   

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders).

It implements the calculator at (https://rusa.org/octime_acp.html) with AJAX and Flask.

The first control closes an hour after the ride begins.

This calculator uses French rules: when a control is at 60 or less km from the start, it closes at 20km/hr + 1hr.

Otherwise, the calculator uses this table (from rusa.org)

| Control location (km) | Minimum Speed (km/hr) | Maximum Speed (km/hr) |
|-----------------------|-----------------------|-----------------------|
| 0 - 200               | 15                    | 34                    |
| 200 - 400             | 15                    | 32                    |
| 400 - 600             | 15                    | 30                    |
| 600 - 1000            | 11.428                | 28                    |

The last control can be at or less than 20% greater than the length of the brevet and closes at:

* 13:30 for 200 KM

* 20:00 for 300 KM

* 27:00 for 400 KM

* 40:00 for 600 KM

* 75:00 for 1000 KM  

## AJAX and Flask reimplementation

The RUSA controle time calculator is a Perl script that takes an HTML form and emits a text page in the above link.

* Each time a distance is filled in, the corresponding open and close times are filled in.

## Functionality added

Creates the following functionality, adding to Project 4. 1) Create two buttons ("Submit") and ("Display") in the page where have controle times. 2) On clicking the Submit button, the control times should be entered into the database. 3) On clicking the Display button, the entries from the database should be displayed in a new page.

## Test cases

1) When submit button is pressed and there are values present in the form, those values are
added to the database.

2) When submit button is pressed and there are no values present in the form, no values are added to
the database.

3) When display button is pressed and there are values present in the database, the page redirects to display those values.

4) When display button is pressed and there are no values present in the database, the page does
not redirect.

## Author

Audra McNamee

amcnamee@uoregon.edu
